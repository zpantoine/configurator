class ConfigurationsController < ApplicationController
	def edit
		@data = JSON.parse(File.read('app/assets/javascripts/config.json'))
	end

	def update
		payline_rows = params[:payline_rows] ? params[:payline_rows].map { |n| n.to_i } : []

		puts(params)

		data = {
			"game":{
				"playfield":{"columns": params[:cols], "rows": params[:rows]},
				"payline": {"type": params[:payline_type], "rows": payline_rows},
				"player":{"startBalance": params[:start_balance], "wager": params[:wager]},
				"symbols": {"spritesheet_url": "assets/spritesheet.png", "count": params[:symbol_count], "info":[{"x":0, "y":0, "width":256, "height":256, "multiplier": params["multiplier"][0]}, {"x":256, "y":0, "width":256, "height":256, "multiplier": params["multiplier"][1]}, {"x":512, "y":0, "width":256, "height":256, "multiplier": params["multiplier"][2]}, {"x":768, "y":0, "width":256, "height":256, "multiplier": params["multiplier"][3]}, {"x":0, "y":256, "width":256, "height":256, "multiplier": params["multiplier"][4]}, {"x":256, "y":256, "width":256, "height":256, "multiplier": params["multiplier"][5]}, {"x":512, "y":256, "width":256, "height":256, "multiplier": params["multiplier"][6]}, {"x":768, "y":256, "width":256, "height":256, "multiplier": params["multiplier"][7]}, {"x":0, "y":512, "width":256, "height":256, "multiplier": params["multiplier"][8]},{"x":256, "y":512, "width":256, "height":256, "multiplier": params["multiplier"][9]}]}
			}
		}

		File.open('app/assets/javascripts/config.json', 'w') do |f|
			f.write(JSON.pretty_generate(data))
		end

		flash[:success] = %Q[Success! <a href="https://zpantoine.bitbucket.io">You can see the app here.</a>]
		redirect_to root_path
	end

	def data
		send_file 'app/assets/javascripts/config.json', :disposition => 'inline'
	end

	def crossdomain
		send_file 'app/assets/xml/crossdomain.xml', :disposition => 'inline'
	end

	def spritesheet
		send_file 'app/assets/images/spritesheet.png', :disposition => 'inline'
	end
end
