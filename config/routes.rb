Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'configurations#edit'
  post '/' => 'configurations#update'
  get 'data' => 'configurations#data'
  get 'crossdomain.xml' => 'configurations#crossdomain'
  get 'spritesheet' => 'configurations#spritesheet'
end
